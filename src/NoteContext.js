import React, { createContext, useReducer } from 'react';

const initialState = {
  notes: [],
};

export const NoteContext = createContext();

export const NoteProvider = ({ children }) => {
  const [state, dispatch] = useReducer(noteReducer, initialState);

  const addNote = (title, content) => {
    const newNote = {
      id: Date.now().toString(),
      title,
      content,
    };
    dispatch({ type: 'ADD_NOTE', payload: newNote });
  };

  const updateNote = (id, title, content) => {
    dispatch({ type: 'UPDATE_NOTE', payload: { id, title, content } });
  };

  const deleteNote = (id) => {
    dispatch({ type: 'DELETE_NOTE', payload: id });
  };

  return (
    <NoteContext.Provider value={{ state, addNote, updateNote, deleteNote }}>
      {children}
    </NoteContext.Provider>
  );
};

const noteReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_NOTE':
      return {
        ...state,
        notes: [...state.notes, action.payload],
      };
    case 'UPDATE_NOTE':
      return {
        ...state,
        notes: state.notes.map((note) =>
          note.id === action.payload.id
            ? { ...note, title: action.payload.title, content: action.payload.content }
            : note
        ),
      };
    case 'DELETE_NOTE':
      return {
        ...state,
        notes: state.notes.filter((note) => note.id !== action.payload),
      };
    default:
      return state;
  }
};