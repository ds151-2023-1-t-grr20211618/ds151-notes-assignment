import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TouchableOpacity, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { NoteProvider, NoteContext } from './src/NoteContext';

const Stack = createStackNavigator();

const HomeScreen = ({ navigation }) => {
  const { state, deleteNote } = React.useContext(NoteContext);

  const deletarAnotacao = (id) => {
    deleteNote(id);
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={state.notes}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.noteItem}
            onPress={() => navigation.navigate('ReadNote', { id: item.id })}
            onLongPress={() => deletarAnotacao(item.id)}
          >
            <Text style={styles.noteTitle}>{item.title}</Text>
            <Text>{item.content}</Text>
          </TouchableOpacity>
        )}
      />
      <Button title="Adicionar uma nova anotação" onPress={() => navigation.navigate('AddNote')} />
    </View>
  );
};

const AddNoteScreen = ({ navigation }) => {
  const { addNote } = React.useContext(NoteContext);
  const [title, setTitle] = React.useState('');
  const [content, setContent] = React.useState('');

  const adicionarAnotacao = () => {
    addNote(title, content);
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Text>Preencha os campos abaixo:</Text>
      <TextInput
        style={styles.input}
        placeholder="Título da Anotação"
        value={title}
        onChangeText={setTitle}
      />
      <TextInput
        style={styles.input}
        placeholder="Conteúdo"
        value={content}
        onChangeText={setContent}
      />
      <Button title="Salvar" onPress={adicionarAnotacao} />
    </View>
  );
};

const ReadNoteScreen = ({ navigation, route }) => {
  const { state } = React.useContext(NoteContext);
  const { id } = route.params;
  const note = state.notes.find((item) => item.id === id);

  if (!note) {
    return (
      <View style={styles.container}>
        <Text>A anotação foi removida com sucesso. Volte para a página inicial.</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.noteTitle}>{note.title}</Text>
      <Text>{note.content}</Text>
      <Button title="Editar" onPress={() => navigation.navigate('EditNote', { id })} />
      <Button title="Remover" onPress={() => navigation.navigate('RemoveNote', { id })} />
    </View>
  );
};

const EditNoteScreen = ({ navigation, route }) => {
  const { state, updateNote } = React.useContext(NoteContext);
  const { id } = route.params;
  const note = state.notes.find((item) => item.id === id);
  const [title, setTitle] = React.useState(note.title);
  const [content, setContent] = React.useState(note.content);

  const atualizarAnotacao = () => {
    updateNote(id, title, content);
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Text>Atualize os campos abaixo:</Text>
      <TextInput
        style={styles.input}
        placeholder="Título da Anotação"
        value={title}
        onChangeText={setTitle}
      />
      <TextInput
        style={styles.input}
        placeholder="Conteúdo"
        value={content}
        onChangeText={setContent}
      />
      <Button title="Salvar" onPress={atualizarAnotacao} />
    </View>
  );
};

const RemoveNoteScreen = ({ navigation, route }) => {
  const { deleteNote } = React.useContext(NoteContext);
  const { id } = route.params;

  const deletarAnotacao = () => {
    deleteNote(id);
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Text>Tem certeza que quer remover? Se sim, clique no botão abaixo.</Text>
      <Button title="Remover Anotação" onPress={deletarAnotacao} />
    </View>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <NoteProvider>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Página Inicial' }}/>
          <Stack.Screen name="AddNote" component={AddNoteScreen} options={{ title: 'Adicionar Anotação' }}/>
          <Stack.Screen name="ReadNote" component={ReadNoteScreen} options={{ title: 'Ler Anotação' }}/>
          <Stack.Screen name="EditNote" component={EditNoteScreen} options={{ title: 'Editar Anotação' }}/>
          <Stack.Screen name="RemoveNote" component={RemoveNoteScreen} options={{ title: 'Excluir Anotação' }}/>
        </Stack.Navigator>
      </NoteProvider>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 16,
  },
  noteItem: {
    marginBottom: 16,
    borderWidth: 2,
    borderColor: 'black',
    padding: 16,
  },
  noteTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    marginBottom: 8,
    padding: 8,
  }
});

export default App;